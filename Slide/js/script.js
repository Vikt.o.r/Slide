//  The main function for finding images and working with them
function solidesPlugin(activeSlides = 0) {
    const slides = document.querySelectorAll('.slide');
    slides[activeSlides].classList.add('active');
    // Cycle for a slide by clicking on an image
    for (const slide of slides) {
        slide.addEventListener('click', () => {
            clearActiveClasses()
            slide.classList.add('active');
        })
    }
    // Function to return the image to its original state
    function clearActiveClasses() {
        slides.forEach((slide) => {
            slide.classList.remove('active');
        })
    }
}
solidesPlugin();
